import java.util.Scanner;
public class OX {
    public static void showWelcome(){
        System.out.print("--------------------------\n"+
                         "|   Welcome to XO Game   |\n"+
                         "--------------------------\n");
    }
    
    public static void showBoard(char[][] board){
        System.out.println();
        for(int i=0;i<3;i++){
            System.out.print("\t");
            for(int j=0;j<3;j++){
                System.out.print("|"+board[i][j]);
            }          
            System.out.println("|");
        }
        System.out.println();
        System.out.println("-------------------------");
    }
    
    public static char switchPlayer(char player){
            if(player=='O'){
                player='X';
                player='X';
                return player;
            }else{
                player='O';
                player='O';
                return player;
            }
    }
    
    public static char addinBoard(int X, int Y,char[][] board,char player){
        if(X>3||Y>3){
            System.out.print("Please Input again Position Out of range!!!\n");
        }else if(board[X-1][Y-1]=='_'){
            board[X-1][Y-1]=player;
            player=switchPlayer(player);
            showBoard(board);
        }else{
            System.out.print("This Box is not empty!!!\n");
        }
        return player;
    }
    
    public static Boolean checkWinCol(int i,char[][] board){
        String temp=""+board[0][i]+board[1][i]+ board[2][i];
        return (temp.equals("XXX")||temp.equals("OOO"));       
    }
    
    public static Boolean checkWinRow(int i,char[][] board){ 
        String temp=""+board[i][0]+board[i][1]+ board[i][2]; 
        return (temp.equals("XXX")||temp.equals("OOO"));
    }
    
    public static Boolean dataSlantX(char[][] board){ 
        String temp=""+board[0][0]+board[1][1]+ board[2][2]; 
        return (temp.equals("XXX")||temp.equals("OOO"));    
    }
    
    public static Boolean dataSlantY(char[][] board){ 
        String temp=""+board[2][0]+board[1][1]+ board[0][2];      
        return (temp.equals("XXX")||temp.equals("OOO")); 
    }
    
    public static boolean winGame(char[][] board){ 
        for(int i=0;i<3;i++){
            if(checkWinRow(i,board)){
                System.out.print("\t"+board[i][0]+" Win\n");
                return true;
            }else if(checkWinCol(i,board)){
                System.out.print("\t"+board[0][i]+" Win\n");
                return true;
            }else if(dataSlantX(board)){
                System.out.print("\t"+board[0][0]+" Win\n");
                return true;
            }else if(dataSlantY(board)){
                System.out.print("\t"+board[1][1]+" Win\n");
                return true;
            }
        }
        return false;
    }
    
    public static boolean drawGame(int countTurn){
        if(countTurn==9){
            System.out.print("\tDraw!!!\n");
            return true;
        }else{
            return false;
        }
    }
    
    public static void main(String[] args){
        char[][] board = {{'_','_','_'},{'_','_','_'},{'_','_','_'}};
        char player='X';
        int countTurn=0;
        boolean win=false;
        boolean draw=false;
        Scanner AA=new Scanner(System.in);
        showWelcome();
        showBoard(board);
        
        while(true){
            countTurn++;
            System.out.println("Please Enter Your Position!!!");
            System.out.print(" Player "+player+"  = ");
            try{
                int PlayerChioceX=AA.nextInt();
                int PlayerChioceY=AA.nextInt();
                if(player==addinBoard(PlayerChioceX, PlayerChioceY,board,player)){
                    countTurn--;
                }else{
                    player=switchPlayer(player);
                }
            }catch(Exception e){
                System.out.println("Please Input Position 1-3 Only");
                AA.nextLine();
                continue;
            }
            win=winGame(board);
            if(win){
                break;
            }
            draw=drawGame(countTurn);
            if(draw){
                break;
            }
        }     
    }
}
